package com.example.jms.sender;

import com.example.jms.model.MessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JmsSender {
    private final JmsTemplate jmsTemplate;

    public void sendMessage(MessageDto message) {
        jmsTemplate.convertAndSend("testDestination", message);
    }
}
