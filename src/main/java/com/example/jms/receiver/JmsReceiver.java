package com.example.jms.receiver;

import com.example.jms.model.MessageDto;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class JmsReceiver {
    @JmsListener(destination = "testDestination", containerFactory = "myFactory")
    public void receiveMessage(MessageDto messageDto) {
        System.out.println(messageDto.getNumber());
        System.out.println(messageDto.getString());
    }
}
