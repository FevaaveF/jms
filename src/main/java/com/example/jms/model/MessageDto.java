package com.example.jms.model;

import lombok.Data;

@Data
public class MessageDto {
    private Long number;
    private String string;
}
