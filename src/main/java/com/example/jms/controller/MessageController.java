package com.example.jms.controller;

import com.example.jms.model.MessageDto;
import com.example.jms.sender.JmsSender;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("message")
public class MessageController {
    private final JmsSender messageSender;

    @PostMapping
    public void sendMessage(@RequestBody MessageDto message) {
        messageSender.sendMessage(message);
    }
}
